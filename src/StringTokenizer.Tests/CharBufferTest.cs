﻿namespace StringTokenizer.Tests;

using NUnit.Framework;
using System.Text;

public class CharBufferTest
{
    readonly char[] Data = "This is some test data.".ToCharArray();
    readonly char[] EmptyArray = new char[0];

    private static char[] CharArray(string s) => s.ToCharArray();

    [Test]
    public void TestCapacity()
    {
        var buffer = new CharBuffer(8);

        Assert.That(buffer.Length, Is.EqualTo(0));
        Assert.That(buffer.Capacity, Is.EqualTo(8));

        buffer.Capacity = 16;

        Assert.That(buffer.Length, Is.EqualTo(0));
        Assert.That(buffer.Capacity, Is.EqualTo(16));
    }

    [Test]
    public void TestStartsWith()
    {
        var buffer = new CharBuffer(Data);

        var needle = CharArray("This ");

        Assert.That(buffer.StartsWith(needle));

        needle = CharArray("some");

        Assert.That(!buffer.StartsWith(needle));

        // True for empty byte array.
        Assert.That(buffer.StartsWith(EmptyArray));

        buffer = new CharBuffer();

        // False for empty buffer.
        Assert.That(!buffer.StartsWith(needle));

        // True for empty buffer and empty byte array.
        Assert.That(buffer.StartsWith(EmptyArray));
    }

    [Test]
    public void TestEndsWith()
    {
        var buffer = new CharBuffer(Data);

        var needle = CharArray("data.");

        Assert.That(buffer.EndsWith(needle));

        needle = CharArray("some");

        Assert.That(!buffer.EndsWith(needle));

        // True for empty byte array.
        Assert.That(buffer.EndsWith(EmptyArray));

        buffer = new CharBuffer();

        // False for empty buffer.
        Assert.That(!buffer.EndsWith(needle));

        // True for empty buffer and empty byte array.
        Assert.That(buffer.EndsWith(EmptyArray));
    }

    [Test]
    public void TestAppend()
    {
        var buffer = new CharBuffer(Data);

        int position = buffer.Length;

        var needle = CharArray("(append)");

        buffer.Append(needle);

        Assert.That(buffer.IndexOf(needle), Is.EqualTo(position));
    }

    [Test]
    public void TestClear()
    {
        var buffer = new CharBuffer(Data);

        buffer.Clear();

        Assert.That(buffer.Length, Is.EqualTo(0));

        var needle = CharArray("(append)");

        buffer.Append(needle);

        Assert.That(buffer.IndexOf(needle), Is.EqualTo(0));
    }

    [Test]
    public void TestContains()
    {
        var buffer = new CharBuffer(Data);

        var needle = CharArray("This ");

        Assert.That(buffer.Contains(needle));

        needle = CharArray("some");

        Assert.That(buffer.Contains(needle));

        needle = CharArray(" data.");

        Assert.That(buffer.Contains(needle));

        needle = CharArray("Data");

        Assert.That(!buffer.Contains(needle));

        // True for empty byte array.
        Assert.That(buffer.Contains(EmptyArray));

        buffer = new CharBuffer();

        // False for empty buffer.
        Assert.That(!buffer.Contains(needle));

        // True for empty buffer and empty byte array.
        Assert.That(buffer.Contains(EmptyArray));
    }

    [Test]
    public void TestIndexOf()
    {
        var buffer = new CharBuffer(Data);

        var needle = CharArray("This ");

        Assert.That(buffer.IndexOf(needle), Is.EqualTo(0));

        needle = CharArray("some");

        Assert.That(buffer.IndexOf(needle), Is.EqualTo(8));

        needle = CharArray(" data.");

        Assert.That(buffer.IndexOf(needle), Is.EqualTo(17));

        needle = CharArray("Data");

        Assert.That(buffer.IndexOf(needle), Is.EqualTo(-1));

        // Test empty byte array.
        Assert.That(buffer.IndexOf(EmptyArray), Is.EqualTo(0));

        buffer = new CharBuffer();

        // Test empty buffer.
        Assert.That(buffer.IndexOf(needle), Is.EqualTo(-1));
    }

    [Test]
    public void TestToString()
    {
        var encoding = Encoding.ASCII;

        var data = CharArray(" Test  ");

        var buffer = new CharBuffer(data);

        Assert.That(buffer.ToString(), Is.EqualTo(" Test  "));

        Assert.That(buffer.ToString(encoding, 2), Is.EqualTo("est  "));

        Assert.That(buffer.ToString(encoding, 2, true), Is.EqualTo("est"));

        buffer.Clear();
        buffer.Append(CharArray("\r\n"));

        Assert.That(buffer.ToString(encoding, 0, true), Is.EqualTo(""));
    }
}
