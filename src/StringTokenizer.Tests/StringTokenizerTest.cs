﻿namespace StringTokenizer.Tests;

using NUnit.Framework;

class StringTokenizerTest
{
    [Test]
    public void TestInt()
    {
        var s = "1 2 3".ToCharArray();

        var tokenizer = new StringTokenizer(s);

        int value;

        Assert.That(tokenizer.Next(out value));
        Assert.That(value, Is.EqualTo(1));

        Assert.That(tokenizer.Next(out value));
        Assert.That(value, Is.EqualTo(2));

        Assert.That(tokenizer.Next(out value));
        Assert.That(value, Is.EqualTo(3));

        Assert.That(!tokenizer.Next(out value));
        Assert.That(!tokenizer.HasNext());
    }

    [Test]
    public void TestDouble()
    {
        var s = "1.1 2.2 3.3".ToCharArray();

        var tokenizer = new StringTokenizer(s);

        double value;

        Assert.That(tokenizer.Next(out value));
        Assert.That(value, Is.EqualTo(1.1));

        Assert.That(tokenizer.Next(out value));
        Assert.That(value, Is.EqualTo(2.2));

        Assert.That(tokenizer.Next(out value));
        Assert.That(value, Is.EqualTo(3.3));

        Assert.That(!tokenizer.Next(out value));
        Assert.That(!tokenizer.HasNext());
    }

    [Test]
    public void TestMixed()
    {
        var s = "1 \"2 3\" 4.5".ToCharArray();

        var tokenizer = new StringTokenizer(s)
        {
            Strategy = new StringTokenizer.QuotedWhitespaceStrategy('"')
        };
        
        Assert.That(tokenizer.Next(out double ival));
        Assert.That(ival, Is.EqualTo(1));

        Assert.That(tokenizer.Next(out string sval));
        Assert.That(sval, Is.EqualTo("2 3"));

        Assert.That(tokenizer.Next(out double dval));
        Assert.That(dval, Is.EqualTo(4.5));

        Assert.That(!tokenizer.HasNext());
    }
}
