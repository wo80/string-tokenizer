﻿namespace StringTokenizer.Tests;

using NUnit.Framework;
using System.Text;

public class ByteBufferTest
{
    readonly byte[] Data = Encoding.ASCII.GetBytes("This is some test data.");
    readonly byte[] EmptyArray = new byte[0];

    [Test]
    public void TestCapacity()
    {
        var buffer = new ByteBuffer(8);

        Assert.That(buffer.Length, Is.EqualTo(0));
        Assert.That(buffer.Capacity, Is.EqualTo(8));

        buffer.Capacity = 16;

        Assert.That(buffer.Length, Is.EqualTo(0));
        Assert.That(buffer.Capacity, Is.EqualTo(16));
    }

    [Test]
    public void TestStartsWith()
    {
        var buffer = new ByteBuffer(Data);

        var needle = Encoding.ASCII.GetBytes("This ");

        Assert.That(buffer.StartsWith(needle));

        needle = Encoding.ASCII.GetBytes("some");

        Assert.That(!buffer.StartsWith(needle));

        // True for empty byte array.
        Assert.That(buffer.StartsWith(EmptyArray));

        buffer = new ByteBuffer();

        // False for empty buffer.
        Assert.That(!buffer.StartsWith(needle));

        // True for empty buffer and empty byte array.
        Assert.That(buffer.StartsWith(EmptyArray));
    }

    [Test]
    public void TestEndsWith()
    {
        var buffer = new ByteBuffer(Data);

        var needle = Encoding.ASCII.GetBytes("data.");

        Assert.That(buffer.EndsWith(needle));

        needle = Encoding.ASCII.GetBytes("some");

        Assert.That(!buffer.EndsWith(needle));

        // True for empty byte array.
        Assert.That(buffer.EndsWith(EmptyArray));

        buffer = new ByteBuffer();

        // False for empty buffer.
        Assert.That(!buffer.EndsWith(needle));

        // True for empty buffer and empty byte array.
        Assert.That(buffer.EndsWith(EmptyArray));
    }

    [Test]
    public void TestAppend()
    {
        var buffer = new ByteBuffer(Data);

        int position = buffer.Length;

        var needle = Encoding.ASCII.GetBytes("(append)");

        buffer.Append(needle);

        Assert.That(buffer.IndexOf(needle), Is.EqualTo(position));
    }

    [Test]
    public void TestClear()
    {
        var buffer = new ByteBuffer(Data);

        buffer.Clear();

        Assert.That(buffer.Length, Is.EqualTo(0));

        var needle = Encoding.ASCII.GetBytes("(append)");

        buffer.Append(needle);

        Assert.That(buffer.IndexOf(needle), Is.EqualTo(0));
    }

    [Test]
    public void TestContains()
    {
        var buffer = new ByteBuffer(Data);

        var needle = Encoding.ASCII.GetBytes("This ");

        Assert.That(buffer.Contains(needle));

        needle = Encoding.ASCII.GetBytes("some");

        Assert.That(buffer.Contains(needle));

        needle = Encoding.ASCII.GetBytes(" data.");

        Assert.That(buffer.Contains(needle));

        needle = Encoding.ASCII.GetBytes("Data");

        Assert.That(!buffer.Contains(needle));

        // True for empty byte array.
        Assert.That(buffer.Contains(EmptyArray));

        buffer = new ByteBuffer();

        // False for empty buffer.
        Assert.That(!buffer.Contains(needle));

        // True for empty buffer and empty byte array.
        Assert.That(buffer.Contains(EmptyArray));
    }

    [Test]
    public void TestIndexOf()
    {
        var buffer = new ByteBuffer(Data);

        var needle = Encoding.ASCII.GetBytes("This ");

        Assert.That(buffer.IndexOf(needle), Is.EqualTo(0));

        needle = Encoding.ASCII.GetBytes("some");

        Assert.That(buffer.IndexOf(needle), Is.EqualTo(8));

        needle = Encoding.ASCII.GetBytes(" data.");

        Assert.That(buffer.IndexOf(needle), Is.EqualTo(17));

        needle = Encoding.ASCII.GetBytes("Data");

        Assert.That(buffer.IndexOf(needle), Is.EqualTo(-1));

        // Test empty byte array.
        Assert.That(buffer.IndexOf(EmptyArray), Is.EqualTo(0));

        buffer = new ByteBuffer();

        // Test empty buffer.
        Assert.That(buffer.IndexOf(needle), Is.EqualTo(-1));
    }

    [Test]
    public void TestToString()
    {
        var encoding = Encoding.ASCII;

        var data = encoding.GetBytes(" Test  ");

        var buffer = new ByteBuffer(data);

        Assert.That(buffer.ToString(), Is.EqualTo(" Test  "));

        Assert.That(buffer.ToString(encoding, 2), Is.EqualTo("est  "));

        Assert.That(buffer.ToString(encoding, 2, true), Is.EqualTo("est"));

        buffer.Clear();
        buffer.Append(encoding.GetBytes("\r\n"));

        Assert.That(buffer.ToString(encoding, 0, true), Is.EqualTo(""));
    }
}
