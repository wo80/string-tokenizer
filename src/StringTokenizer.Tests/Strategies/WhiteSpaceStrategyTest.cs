﻿namespace StringTokenizer.Tests.Strategies;

using NUnit.Framework;

class WhiteSpaceStrategyTest
{
    [Test]
    public void TestWhiteSpace()
    {
        var s = "1 2 3".ToCharArray();

        var tokenizer = new StringTokenizer(s);

        var span = tokenizer.Next();

        Assert.That(span.Start, Is.EqualTo(0));
        Assert.That(span.End, Is.EqualTo(1));

        Assert.That(tokenizer.HasNext());

        span = tokenizer.Next();

        Assert.That(span.Start, Is.EqualTo(2));
        Assert.That(span.End, Is.EqualTo(3));

        Assert.That(tokenizer.HasNext());

        span = tokenizer.Next();

        Assert.That(span.Start, Is.EqualTo(4));
        Assert.That(span.End, Is.EqualTo(5));

        Assert.That(!tokenizer.HasNext());

        span = tokenizer.Next();

        Assert.That(span.IsEmpty());
    }

    [Test]
    public void TestWhiteSpaceRight()
    {
        var s = "1 2 3 ".ToCharArray();

        var tokenizer = new StringTokenizer(s);

        var span = tokenizer.Next();

        Assert.That(span.Start, Is.EqualTo(0));
        Assert.That(span.End, Is.EqualTo(1));

        span = tokenizer.Next();

        Assert.That(span.Start, Is.EqualTo(2));
        Assert.That(span.End, Is.EqualTo(3));

        span = tokenizer.Next();

        Assert.That(span.Start, Is.EqualTo(4));
        Assert.That(span.End, Is.EqualTo(5));

        span = tokenizer.Next();

        Assert.That(span.IsEmpty());
    }

    [Test]
    public void TestWhiteSpaceLeft()
    {
        var s = " 1 2 3".ToCharArray();

        var tokenizer = new StringTokenizer(s);

        var span = tokenizer.Next();

        Assert.That(span.Start, Is.EqualTo(1));
        Assert.That(span.End, Is.EqualTo(2));

        span = tokenizer.Next();

        Assert.That(span.Start, Is.EqualTo(3));
        Assert.That(span.End, Is.EqualTo(4));

        span = tokenizer.Next();

        Assert.That(span.Start, Is.EqualTo(5));
        Assert.That(span.End, Is.EqualTo(6));

        span = tokenizer.Next();

        Assert.That(span.IsEmpty());
    }

    [Test]
    public void TestWhiteSpaceTabs()
    {
        var s = "1\t2\t3".ToCharArray();

        var tokenizer = new StringTokenizer(s);

        var span = tokenizer.Next();

        Assert.That(span.Start, Is.EqualTo(0));
        Assert.That(span.End, Is.EqualTo(1));

        span = tokenizer.Next();

        Assert.That(span.Start, Is.EqualTo(2));
        Assert.That(span.End, Is.EqualTo(3));

        span = tokenizer.Next();

        Assert.That(span.Start, Is.EqualTo(4));
        Assert.That(span.End, Is.EqualTo(5));

        span = tokenizer.Next();

        Assert.That(span.IsEmpty());
    }

    [Test]
    public void TestWhiteSpaceTabsRight()
    {
        var s = "1\t2\t3\t".ToCharArray();

        var tokenizer = new StringTokenizer(s);

        var span = tokenizer.Next();

        Assert.That(span.Start, Is.EqualTo(0));
        Assert.That(span.End, Is.EqualTo(1));

        span = tokenizer.Next();

        Assert.That(span.Start, Is.EqualTo(2));
        Assert.That(span.End, Is.EqualTo(3));

        span = tokenizer.Next();

        Assert.That(span.Start, Is.EqualTo(4));
        Assert.That(span.End, Is.EqualTo(5));

        span = tokenizer.Next();

        Assert.That(span.IsEmpty());
    }

    [Test]
    public void TestWhiteSpaceTabsLeft()
    {
        var s = "\t1\t2\t3".ToCharArray();

        var tokenizer = new StringTokenizer(s);

        var span = tokenizer.Next();

        Assert.That(span.Start, Is.EqualTo(1));
        Assert.That(span.End, Is.EqualTo(2));

        span = tokenizer.Next();

        Assert.That(span.Start, Is.EqualTo(3));
        Assert.That(span.End, Is.EqualTo(4));

        span = tokenizer.Next();

        Assert.That(span.Start, Is.EqualTo(5));
        Assert.That(span.End, Is.EqualTo(6));

        span = tokenizer.Next();

        Assert.That(span.IsEmpty());
    }

    [Test]
    public void TestWhiteSpaceMix()
    {
        var s = "\t  1 \t 22 \t 333 ".ToCharArray();

        var tokenizer = new StringTokenizer(s);

        var span = tokenizer.Next();

        Assert.That(span.Start, Is.EqualTo(3));
        Assert.That(span.End, Is.EqualTo(4));

        span = tokenizer.Next();

        Assert.That(span.Start, Is.EqualTo(7));
        Assert.That(span.End, Is.EqualTo(9));

        span = tokenizer.Next();

        Assert.That(span.Start, Is.EqualTo(12));
        Assert.That(span.End, Is.EqualTo(15));

        span = tokenizer.Next();

        Assert.That(span.IsEmpty());
    }
}
