﻿namespace StringTokenizer.Tests.Strategies;

using NUnit.Framework;

class DelimiterStrategyTest
{
    [Test]
    public void TestDelimiter()
    {
        var s = "1;2;3".ToCharArray();

        var tokenizer = new StringTokenizer(s)
        {
            Strategy = new StringTokenizer.DelimiterStrategy(';')
        };

        var span = tokenizer.Next();

        Assert.That(span.Start, Is.EqualTo(0));
        Assert.That(span.End, Is.EqualTo(1));

        Assert.That(tokenizer.HasNext());

        span = tokenizer.Next();

        Assert.That(span.Start, Is.EqualTo(2));
        Assert.That(span.End, Is.EqualTo(3));

        Assert.That(tokenizer.HasNext());

        span = tokenizer.Next();

        Assert.That(span.Start, Is.EqualTo(4));
        Assert.That(span.End, Is.EqualTo(5));

        Assert.That(!tokenizer.HasNext());

        span = tokenizer.Next();

        Assert.That(span.IsEmpty());
    }

    [Test]
    public void TestDelimiterAtEnd()
    {
        var s = "1;2;".ToCharArray();

        var tokenizer = new StringTokenizer(s)
        {
            Strategy = new StringTokenizer.DelimiterStrategy(';')
        };

        var span = tokenizer.Next();

        Assert.That(span.Start, Is.EqualTo(0));
        Assert.That(span.End, Is.EqualTo(1));

        Assert.That(tokenizer.HasNext());

        span = tokenizer.Next();

        Assert.That(span.Start, Is.EqualTo(2));
        Assert.That(span.End, Is.EqualTo(3));

        Assert.That(!tokenizer.HasNext());

        span = tokenizer.Next();

        Assert.That(span.IsEmpty());
    }

    [Test]
    public void TestDelimiterEmptyToken()
    {
        var s = "1;;".ToCharArray();

        var tokenizer = new StringTokenizer(s)
        {
            Strategy = new StringTokenizer.DelimiterStrategy(';')
        };

        var span = tokenizer.Next();

        Assert.That(span.Start, Is.EqualTo(0));
        Assert.That(span.End, Is.EqualTo(1));

        Assert.That(tokenizer.HasNext());

        span = tokenizer.Next();

        Assert.That(span.Start, Is.EqualTo(2));
        Assert.That(span.End, Is.EqualTo(2));

        Assert.That(!tokenizer.HasNext());
    }

    [Test]
    public void TestDelimiterTrim()
    {
        var s = " 1 ; 2 ; 3".ToCharArray();

        var tokenizer = new StringTokenizer(s)
        {
            Strategy = new StringTokenizer.DelimiterStrategy(';')
        };

        var span = tokenizer.Next();

        Assert.That(span.Start, Is.EqualTo(1));
        Assert.That(span.End, Is.EqualTo(2));

        span = tokenizer.Next();

        Assert.That(span.Start, Is.EqualTo(5));
        Assert.That(span.End, Is.EqualTo(6));

        span = tokenizer.Next();

        Assert.That(span.Start, Is.EqualTo(9));
        Assert.That(span.End, Is.EqualTo(10));

        span = tokenizer.Next();

        Assert.That(span.IsEmpty());
    }

    [Test]
    public void TestDelimiterNoTrim()
    {
        var s = " 1 ; 2 ; 3".ToCharArray();

        var tokenizer = new StringTokenizer(s)
        {
            Strategy = new StringTokenizer.DelimiterStrategy(';', false)
        };

        var span = tokenizer.Next();

        Assert.That(span.Start, Is.EqualTo(0));
        Assert.That(span.End, Is.EqualTo(3));

        span = tokenizer.Next();

        Assert.That(span.Start, Is.EqualTo(4));
        Assert.That(span.End, Is.EqualTo(7));

        span = tokenizer.Next();

        Assert.That(span.Start, Is.EqualTo(8));
        Assert.That(span.End, Is.EqualTo(10));

        span = tokenizer.Next();

        Assert.That(span.IsEmpty());
    }

    [Test]
    public void TestDelimiterNoTrimAsInt32()
    {
        var s = " 1 ; 2 ; 3".ToCharArray();

        var tokenizer = new StringTokenizer(s)
        {
            Strategy = new StringTokenizer.DelimiterStrategy(';', false)
        };

        Assert.That(tokenizer.Next(out int i));
        Assert.That(i, Is.EqualTo(1));

        Assert.That(tokenizer.Next(out i));
        Assert.That(i, Is.EqualTo(2));

        Assert.That(tokenizer.Next(out i));
        Assert.That(i, Is.EqualTo(3));
    }

    [Test]
    public void TestDelimiterNoTrimAsDouble()
    {
        var s = " 1.1 ; 2.2 ; 3.3".ToCharArray();

        var tokenizer = new StringTokenizer(s)
        {
            Strategy = new StringTokenizer.DelimiterStrategy(';', false)
        };

        Assert.That(tokenizer.Next(out double i));
        Assert.That(i, Is.EqualTo(1.1));

        Assert.That(tokenizer.Next(out i));
        Assert.That(i, Is.EqualTo(2.2));

        Assert.That(tokenizer.Next(out i));
        Assert.That(i, Is.EqualTo(3.3));
    }
}
