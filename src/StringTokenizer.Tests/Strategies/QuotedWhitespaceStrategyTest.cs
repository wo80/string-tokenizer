﻿namespace StringTokenizer.Tests.Strategies;

using NUnit.Framework;

class QuotedWhitespaceStrategyTest
{
    [Test]
    public void TestQuotedWhiteSpace()
    {
        var s = "1 \"2.1 2.2\" 3".ToCharArray();

        var tokenizer = new StringTokenizer(s)
        {
            Strategy = new StringTokenizer.QuotedWhitespaceStrategy('"')
        };

        var span = tokenizer.Next();

        Assert.That(span.Start, Is.EqualTo(0));
        Assert.That(span.End, Is.EqualTo(1));

        span = tokenizer.Next();

        Assert.That(span.Start, Is.EqualTo(3));
        Assert.That(span.End, Is.EqualTo(10));

        span = tokenizer.Next();

        Assert.That(span.Start, Is.EqualTo(12));
        Assert.That(span.End, Is.EqualTo(13));

        span = tokenizer.Next();

        Assert.That(span.IsEmpty());
    }

    [Test]
    public void TestQuotedWhiteSpaceMismatch()
    {
        var s = "1 '2.1 2.2' 3".ToCharArray();

        // Using a quote that doesn't match the string.
        var tokenizer = new StringTokenizer(s)
        {
            Strategy = new StringTokenizer.QuotedWhitespaceStrategy('"')
        };

        var span = tokenizer.Next();

        Assert.That(span.Start, Is.EqualTo(0));
        Assert.That(span.End, Is.EqualTo(1));

        span = tokenizer.Next();

        Assert.That(span.Start, Is.EqualTo(2));
        Assert.That(span.End, Is.EqualTo(6));

        span = tokenizer.Next();

        Assert.That(span.Start, Is.EqualTo(7));
        Assert.That(span.End, Is.EqualTo(11));

        span = tokenizer.Next();

        Assert.That(span.Start, Is.EqualTo(12));
        Assert.That(span.End, Is.EqualTo(13));

        span = tokenizer.Next();

        Assert.That(span.IsEmpty());
    }

    [Test]
    public void TestQuotedWhiteSpaceLeft()
    {
        var s = "\"1.1 1.2\" 2 3".ToCharArray();
        
        var tokenizer = new StringTokenizer(s)
        {
            Strategy = new StringTokenizer.QuotedWhitespaceStrategy('"')
        };

        var span = tokenizer.Next();

        Assert.That(span.Start, Is.EqualTo(1));
        Assert.That(span.End, Is.EqualTo(8));

        span = tokenizer.Next();

        Assert.That(span.Start, Is.EqualTo(10));
        Assert.That(span.End, Is.EqualTo(11));

        span = tokenizer.Next();

        Assert.That(span.Start, Is.EqualTo(12));
        Assert.That(span.End, Is.EqualTo(13));

        span = tokenizer.Next();

        Assert.That(span.IsEmpty());
    }

    [Test]
    public void TestQuotedWhiteSpaceRight()
    {
        var s = "1 2 \"3.1 3.2\" ".ToCharArray();
        
        var tokenizer = new StringTokenizer(s)
        {
            Strategy = new StringTokenizer.QuotedWhitespaceStrategy('"')
        };

        var span = tokenizer.Next();

        Assert.That(span.Start, Is.EqualTo(0));
        Assert.That(span.End, Is.EqualTo(1));

        span = tokenizer.Next();

        Assert.That(span.Start, Is.EqualTo(2));
        Assert.That(span.End, Is.EqualTo(3));

        span = tokenizer.Next();

        Assert.That(span.Start, Is.EqualTo(5));
        Assert.That(span.End, Is.EqualTo(12));

        span = tokenizer.Next();

        Assert.That(span.IsEmpty());
    }

    [Test]
    public void TestQuotedWhiteSpace2()
    {
        var s = "1 (2.1 2.2) 3".ToCharArray();

        // Using different opening and closing quotes
        var tokenizer = new StringTokenizer(s)
        {
            Strategy = new StringTokenizer.QuotedWhitespaceStrategy('(', ')')
        };

        var span = tokenizer.Next();

        Assert.That(span.Start, Is.EqualTo(0));
        Assert.That(span.End, Is.EqualTo(1));

        span = tokenizer.Next();

        Assert.That(span.Start, Is.EqualTo(3));
        Assert.That(span.End, Is.EqualTo(10));

        span = tokenizer.Next();

        Assert.That(span.Start, Is.EqualTo(12));
        Assert.That(span.End, Is.EqualTo(13));

        span = tokenizer.Next();

        Assert.That(span.IsEmpty());
    }

    [Test]
    public void TestQuotedWhiteSpaceNotClosed()
    {
        var s = "1 \"2 3".ToCharArray();
        
        var tokenizer = new StringTokenizer(s)
        {
            Strategy = new StringTokenizer.QuotedWhitespaceStrategy('"')
        };

        var span = tokenizer.Next();

        Assert.That(span.Start, Is.EqualTo(0));
        Assert.That(span.End, Is.EqualTo(1));

        span = tokenizer.Next();

        Assert.That(span.Start, Is.EqualTo(3));
        Assert.That(span.End, Is.EqualTo(6));

        span = tokenizer.Next();

        Assert.That(span.IsEmpty());
    }
}
