﻿namespace StringTokenizer.Tests;


using NUnit.Framework;
using System;
using System.Collections.Generic;

[DefaultFloatingPointTolerance(1e-16)]
public class ParseTest
{
    [Test]
    public void TestParseDouble()
    {
        Dictionary<string, double> tests = new()
        {
            { "20", 20 },
            { "20.0", 20 },
            { "0.5", 0.5 },
            // Number with lots of digits.
            { "+1234567890.0123456789", 1234567890.0123456789 },
            // Number with lots of digits in integer part (would overflow int64).
            { "+12345678909876543210.0", 12345678909876543210.0 },
            // Number with lots of digits in fraction.
            { "-0.032020907703941316020000000000", -0.03202090770394131602 },
            // Number with lots of digits, but small significant part.
            { "-0.00000000000000000000123", -0.00000000000000000000123 }
        };

        foreach (var item in tests)
        {
            var input = item.Key;
            var expected = item.Value;

            Assert.That(Parse.Double(input, out double actual));
            Assert.That(actual, Is.EqualTo(expected));
        }
    }

    [Test]
    public void TestParseDoubleSigned()
    {
        Dictionary<string, double> tests = new()
        {
            { "-20", -20 },
            { "-20.0", -20 },
            { "-0.5", -0.5 },
            { "+0.5", +0.5 }
        };

        foreach (var item in tests)
        {
            var input = item.Key;
            var expected = item.Value;

            Assert.That(Parse.Double(input, out double actual));
            Assert.That(actual, Is.EqualTo(expected));
        }
    }

    [Test]
    public void TestParseDoubleDot()
    {
        Dictionary<string, double> tests = new()
        {
            { ".5", 0.5 },
            { "-.5", -0.5 },
            { "-.05", -0.05 },
            { "+.5", +0.5 },
            { "+.05", +0.05 }
        };

        foreach (var item in tests)
        {
            var input = item.Key;
            var expected = item.Value;

            Assert.That(Parse.Double(input, out double actual));
            Assert.That(actual, Is.EqualTo(expected));
        }
    }

    [Test]
    public void TestParseDoubleExp()
    {
        Dictionary<string, double> tests = new()
        {
            { ".5e-2", 0.005 },
            { "-.5e-2", -0.005 },
            { "+.5e-2", +0.005 },
            { "5e-2", 0.05 },
            { "-5e-2", -0.05 },
            { "+5e-2", +0.05 },
            { "5.0e-2", 0.05 },
            { "-5.0e-2", -0.05 },
            { "+5.0e-2", +0.05 },
            { "5e2", 500 },
            { "5e+2", 500 },
            { "-5e+2", -500 },
            { "+5e+2", 500 },
            { "5.0e+2", 500 },
            { "-5.0e+2", -500 },
            { "+5.0e+2", +500 }
        };

        foreach (var item in tests)
        {
            var input = item.Key;
            var expected = item.Value;

            Assert.That(Parse.Double(input, out double actual));
            Assert.That(actual, Is.EqualTo(expected));

            // Upper case exponent.

            input = input.Replace('e', 'E');

            Assert.That(Parse.Double(input, out actual));
            Assert.That(actual, Is.EqualTo(expected));
        }
    }

    [Test]
    public void TestParseInt32()
    {
        Dictionary<string, int> tests = new()
        {
            { " 0",  0 },
            { "+1",  1 },
            { "-1", -1 },
            { int.MinValue.ToString(), int.MinValue },
            { int.MaxValue.ToString(), int.MaxValue }
        };

        foreach (var item in tests)
        {
            var input = item.Key;
            var expected = item.Value;

            Assert.That(Parse.Int32(input, out int actual));
            Assert.That(actual, Is.EqualTo(expected), input);
        }

        Assert.Throws<OverflowException>(() =>
        {
            Parse.Int32(long.MaxValue.ToString(), out int actual);
        });
    }

    [Test]
    public void TestParseInt64()
    {
        Dictionary<string, long> tests = new()
        {
            { " 0",  0L },
            { "+1",  1L },
            { "-1", -1L },
            { long.MinValue.ToString(), long.MinValue },
            { long.MaxValue.ToString(), long.MaxValue }
        };

        foreach (var item in tests)
        {
            var input = item.Key;
            var expected = item.Value;

            Assert.That(Parse.Int64(input, out long actual));
            Assert.That(actual, Is.EqualTo(expected), input);
        }

        Assert.That(!Parse.Int64("01", out long _));
    }
}
