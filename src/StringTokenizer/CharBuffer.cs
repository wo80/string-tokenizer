﻿namespace StringTokenizer;

using System.Text;

public class CharBuffer : Buffer<char>
{
    #region Constructors

    /// <inheritdoc/>
    public CharBuffer()
        : base(DefaultCapacity, int.MaxValue)
    {
    }

    /// <inheritdoc/>
    public CharBuffer(int capacity)
        : base(capacity, int.MaxValue)
    {
    }

    /// <inheritdoc/>
    public CharBuffer(int capacity, int maxCapacity)
        : base(capacity, maxCapacity)
    {
    }

    /// <inheritdoc/>
    public CharBuffer(char[] value)
        : base(value, value?.Length ?? DefaultCapacity)
    {
    }

    /// <inheritdoc/>
    public CharBuffer(char[] value, int capacity)
        : base(value, capacity)
    {
    }

    #endregion

    /// <inheritdoc/>
    public override string ToString(Encoding encoding, int startIndex, int count, bool trim)
    {
        if (trim)
        {
            int k = 0;

            while (IsWhitespace(_buffer[startIndex + k]) && k < count) k++;

            startIndex += k;
            count -= k;

            while (IsWhitespace(_buffer[startIndex + count - 1]) && count > 0) count--;
        }

        return new string(_buffer, startIndex, count);
    }

    private static bool IsWhitespace(char b)
    {
        return b == ' ' || b == '\t' || b == '\r' || b == '\n';
    }
}
