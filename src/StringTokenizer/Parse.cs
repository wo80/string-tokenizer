﻿namespace StringTokenizer;

using System;

/// <summary>
/// Convert string representations of a number to its integer or floating-point number equivalent.
/// </summary>
public static class Parse
{
    /// <summary>
    /// Gets or sets the character to use as the decimal separator in numeric values.
    /// </summary>
    public static char DecimalSeparator = '.';

    #region Int32

    /// <summary>
    /// Converts the string representation of a number to its 32bit integer equivalent.
    /// </summary>
    /// <param name="s">A string containing a number to convert. </param>
    /// <param name="result">Contains the integer equivalent of the given string (or zero if the conversion failed).</param>
    /// <returns>Returns a boolean indicating whether the conversion succeeded or failed.</returns>
    public static bool Int32(string s, out int result)
    {
        return Int32(s.AsSpan(), out result);
    }

    /// <summary>
    /// Converts the string representation of a number to its 32bit integer equivalent.
    /// </summary>
    /// <param name="s">A string containing a number to convert. </param>
    /// <param name="start">The start index of the number in string s.</param>
    /// <param name="end">The end index of the number in string s.</param>
    /// <param name="result">Contains the integer equivalent of the given string (or zero if the conversion failed).</param>
    /// <returns>Returns a boolean indicating whether the conversion succeeded or failed.</returns>
    public static bool Int32(string s, int start, int end, out int result)
    {
        result = 0;

        if (start >= end || end > s.Length)
        {
            return false;
        }

        return Int32(s.AsSpan(start, end - start), out result);
    }

    /// <summary>
    /// Converts the string representation of a number to its 32bit integer equivalent.
    /// </summary>
    /// <param name="buffer">A character buffer containing a number to convert. </param>
    /// <param name="start">The start index of the number in string s.</param>
    /// <param name="end">The end index of the number in string s.</param>
    /// <param name="result">Contains the integer equivalent of the given string (or zero if the conversion failed).</param>
    /// <returns>Returns a boolean indicating whether the conversion succeeded or failed.</returns>
    public static bool Int32(char[] buffer, int start, int end, out int result)
    {
        result = 0;

        if (start >= end || end > buffer.Length)
        {
            return false;
        }

        return Int32(buffer.AsSpan(start, end - start), out result);
    }

    /// <summary>
    /// Converts the string representation of a number to its 32bit integer equivalent.
    /// </summary>
    /// <param name="buffer">A character buffer containing a number to convert. </param>
    /// <param name="result">Contains the integer equivalent of the given string (or zero if the conversion failed).</param>
    /// <returns>Returns a boolean indicating whether the conversion succeeded or failed.</returns>
    public static bool Int32(ReadOnlySpan<char> buffer, out int result)
    {
        bool success = Int64(buffer, out long t);

        checked
        {
            result = (int)t;
        }

        return success;
    }

    #endregion

    #region Int64

    /// <summary>
    /// Converts the string representation of a number to its 64bit integer equivalent.
    /// </summary>
    /// <param name="s">A string containing a number to convert. </param>
    /// <param name="result">Contains the integer equivalent of the given string (or zero if the conversion failed).</param>
    /// <returns>Returns a boolean indicating whether the conversion succeeded or failed.</returns>
    public static bool Int64(string s, out long result)
    {
        return Int64(s.AsSpan(), out result);
    }

    /// <summary>
    /// Converts the string representation of a number to its 64bit integer equivalent.
    /// </summary>
    /// <param name="s">A string containing a number to convert. </param>
    /// <param name="start">The start index of the number in string s.</param>
    /// <param name="end">The end index of the number in string s.</param>
    /// <param name="result">Contains the integer equivalent of the given string (or zero if the conversion failed).</param>
    /// <returns>Returns a boolean indicating whether the conversion succeeded or failed.</returns>
    public static bool Int64(string s, int start, int end, out long result)
    {
        result = 0;

        if (start >= end || end > s.Length)
        {
            return false;
        }

        return Int64(s.AsSpan(start, end - start), out result);
    }

    /// <summary>
    /// Converts the string representation of a number to its 64bit integer equivalent.
    /// </summary>
    /// <param name="buffer">A character buffer containing a number to convert. </param>
    /// <param name="start">The start index of the number in string s.</param>
    /// <param name="end">The end index of the number in string s.</param>
    /// <param name="result">Contains the integer equivalent of the given string (or zero if the conversion failed).</param>
    /// <returns>Returns a boolean indicating whether the conversion succeeded or failed.</returns>
    public static bool Int64(char[] buffer, int start, int end, out long result)
    {
        result = 0;

        if (start >= end || end > buffer.Length)
        {
            return false;
        }

        return Int64(buffer.AsSpan(start, end - start), out result);
    }

    /// <summary>
    /// Converts the string representation of a number to its 64bit integer equivalent.
    /// </summary>
    /// <param name="buffer">A character buffer containing a number to convert. </param>
    /// <param name="start">The start index of the number in string s.</param>
    /// <param name="end">The end index of the number in string s.</param>
    /// <param name="result">Contains the integer equivalent of the given string (or zero if the conversion failed).</param>
    /// <returns>Returns a boolean indicating whether the conversion succeeded or failed.</returns>
    public static bool Int64(ReadOnlySpan<char> buffer, out long result)
    {
        result = 0L;

        int i = 0;
        int end = buffer.Length;

        if (end == 0) return false;

        while (i < end && char.IsWhiteSpace(buffer[i])) i++;

        long sign = 1L;

        char c = buffer[i];

        if (c == '-')
        {
            sign = -1L;
            i++;
        }
        else if (c == '+')
        {
            i++;
        }

        c = buffer[i];

        if (i == end || !char.IsDigit(c)) return false;

        if (i < end - 1 && c == '0' && char.IsDigit(buffer[i])) return false;

        while (i < end)
        {
            c = buffer[i];

            if (!char.IsDigit(c))
            {
                break;
            }

            result = result * 10L + (c - '0');

            i++;
        }

        result = sign * result;

        return true;
    }

    #endregion

    #region Single

    /// <summary>
    /// Converts the string representation of a number to its single-precision floating-point number equivalent.
    /// </summary>
    /// <param name="s">A string containing a number to convert.</param>
    /// <param name="result">Contains the single-precision floating-point number equivalent of the given string
    /// (or zero if the conversion failed).</param>
    /// <returns>Returns a boolean indicating whether the conversion succeeded or failed.</returns>
    public static bool Single(string s, out float result)
    {
        return Single(s.AsSpan(), out result);
    }

    /// <summary>
    /// Converts the string representation of a number to its single-precision floating-point number equivalent.
    /// </summary>
    /// <param name="s">A string containing a number to convert. </param>
    /// <param name="start">The start index of the number in string s.</param>
    /// <param name="end">The end index of the number in string s.</param>
    /// <param name="result">Contains the single-precision floating-point number equivalent of the given string
    /// (or zero if the conversion failed).</param>
    /// <returns>Returns a boolean indicating whether the conversion succeeded or failed.</returns>
    public static bool Single(string s, int start, int end, out float result)
    {
        return Single(s.AsSpan(start, end - start), out result);
    }

    /// <summary>
    /// Converts the string representation of a number to its single-precision floating-point number equivalent.
    /// </summary>
    /// <param name="buffer">A character buffer containing a number to convert. </param>
    /// <param name="start">The start index of the number in string s.</param>
    /// <param name="end">The end index of the number in string s.</param>
    /// <param name="result">Contains the single-precision floating-point number equivalent of the given string
    /// (or zero if the conversion failed).</param>
    /// <returns>Returns a boolean indicating whether the conversion succeeded or failed.</returns>
    public static bool Single(char[] buffer, int start, int end, out float result)
    {
        return Single(buffer.AsSpan(start, end - start), out result);
    }

    /// <summary>
    /// Converts the string representation of a number to its single-precision floating-point number equivalent.
    /// </summary>
    /// <param name="buffer">A character buffer containing a number to convert. </param>
    /// <param name="start">The start index of the number in string s.</param>
    /// <param name="end">The end index of the number in string s.</param>
    /// <param name="result">Contains the single-precision floating-point number equivalent of the given string
    /// (or zero if the conversion failed).</param>
    /// <returns>Returns a boolean indicating whether the conversion succeeded or failed.</returns>
    public static bool Single(ReadOnlySpan<char> buffer, out float result)
    {
        // Single precision has < 9 significant digits.
        bool success = ParseFloat(9, DecimalSeparator, buffer, out double t);

        result = (float)t;

        return success;
    }

    #endregion

    #region Double

    /// <summary>
    /// Converts the string representation of a number to its double-precision floating-point number equivalent.
    /// </summary>
    /// <param name="s">A string containing a number to convert. </param>
    /// <param name="result">Contains the double-precision floating-point number equivalent of the given string
    /// (or zero if the conversion failed).</param>
    /// <returns>Returns a boolean indicating whether the conversion succeeded or failed.</returns>
    public static bool Double(string s, out double result)
    {
        return Double(s.AsSpan(), out result);
    }
    
    /// <summary>
    /// Converts the string representation of a number to its double-precision floating-point number equivalent.
    /// </summary>
    /// <param name="s">A string containing a number to convert. </param>
    /// <param name="start">The start index of the number in string s.</param>
    /// <param name="end">The end index of the number in string s.</param>
    /// <param name="result">Contains the double-precision floating-point number equivalent of the given string
    /// (or zero if the conversion failed).</param>
    /// <returns>Returns a boolean indicating whether the conversion succeeded or failed.</returns>
    public static bool Double(string s, int start, int end, out double result)
    {
        return Double(s.AsSpan(start, end - start), out result);
    }
    
    /// <summary>
    /// Converts the string representation of a number to its double-precision floating-point number equivalent.
    /// </summary>
    /// <param name="buffer">A character buffer containing a number to convert. </param>
    /// <param name="start">The start index of the number in string s.</param>
    /// <param name="end">The end index of the number in string s.</param>
    /// <param name="result">Contains the double-precision floating-point number equivalent of the given string
    /// (or zero if the conversion failed).</param>
    /// <returns>Returns a boolean indicating whether the conversion succeeded or failed.</returns>
    public static bool Double(char[] buffer, int start, int end, out double result)
    {
        // Double precision has < 18 significant digits.
        return Double(buffer.AsSpan(start, end - start), out result);
    }
    
    /// <summary>
    /// Converts the string representation of a number to its double-precision floating-point number equivalent.
    /// </summary>
    /// <param name="buffer">A character buffer containing a number to convert. </param>
    /// <param name="result">Contains the double-precision floating-point number equivalent of the given string
    /// (or zero if the conversion failed).</param>
    /// <returns>Returns a boolean indicating whether the conversion succeeded or failed.</returns>
    public static bool Double(ReadOnlySpan<char> buffer, out double result)
    {
        // Double precision has < 18 significant digits.
        return ParseFloat(18, DecimalSeparator, buffer, out result);
    }

    #endregion

    private static bool ParseFloat(int digits, char seperator, ReadOnlySpan<char> buffer, out double result)
    {
        int end = buffer.Length;

        result = 0.0;

        if (end == 0) return false;

        int sign = 1;

        long rtop = 0L;
        long rbot = 0L;
        int rexp = 0;
        int digit;

        int position = 0;

        while (position < end && buffer[position] < 33)
        {
            ++position; // Skip whitespace.
        }

        char c = buffer[position];

        // Check for sign.
        if (c == '-')
        {
            sign = -1;
            position++;
        }
        else if (c == '+')
        {
            position++;
        }

        // Consume integer part.
        while (position < end)
        {
            c = buffer[position];

            digit = c - 48; // '0'

            if (digit < 0 || digit > 9)
            {
                break;
            }

            if (digits > 0)
            {
                rtop = digit + (rtop * 10);
            }

            position++;
            digits--;
        }

        if (digits <= 0)
        {
            // No significant digits left - return!
            result = sign * rtop * pow10[-digits];

            return true;
        }

        // The index into lookup table for the fractional part.
        int power = 0;

        if (c == seperator)
        {
            position++;

            power = 0;

            // Consume fractional part.
            while (position < end)
            {
                c = buffer[position];

                digit = c - 48; // '0'

                if (digit < 0 || digit > 9)
                {
                    break;
                }

                if (digits > 0)
                {
                    rbot = digit + (rbot * 10);
                    power++;
                }

                position++;

                if (rbot > 0)
                {
                    // Only decrease the number of digits left, if the
                    // fractional part contains data.
                    digits--;
                }
            }
        }

        // Check for exponential.
        if (c == 'e' || c == 'E' || c == 'd' || c == 'D')
        {
            position++;

            c = buffer[position];

            bool positive = true;

            // Check for sign.
            if (c == '-')
            {
                positive = false;
                position++;
            }
            else if (c == '+')
            {
                position++;
            }

            // Consume exponential part.
            while (position < end)
            {
                c = buffer[position];

                digit = c - 48; // '0'

                if (digit < 0 || digit > 9)
                {
                    break;
                }

                rexp = digit + (rexp * 10);

                position++;
            }

            if (positive)
            {
                result = sign * (rtop + rbot * pow10i[power]) * pow10[rexp];
            }
            else
            {

                result = sign * (rtop + rbot * pow10i[power]) * pow10i[rexp];
            }

            return true;
        }

        result = sign * (rtop + rbot * pow10i[power]);

        return true;
    }

    #region Lookup Tables

    private readonly static double[] pow10i = GetDoubleExponentsI();

    private static double[] GetDoubleExponentsI()
    {
        const int MAX = 309;

        var exps = new double[MAX];

        double p = 1.0;

        for (int i = 0; i < MAX; i++)
        {
            exps[i] = p;
            p /= 10;
        }

        return exps;
    }

    private static readonly double[] pow10 = GetDoubleExponents();

    private static double[] GetDoubleExponents()
    {
        const int MAX = 309;

        var exps = new double[MAX];

        double p = 1.0;

        for (var i = 0; i < MAX; i++)
        {
            exps[i] = p;
            p *= 10;
        }

        return exps;
    }

    #endregion
}
