﻿namespace StringTokenizer;

using System.Buffers;
using System.IO;
using System.Text;

public static class TextReaderExtensions
{
    /// <summary>
    /// Reads a line of characters from the text reader and adds it to the <see cref="CharBuffer"/> instance.
    /// </summary>
    /// <param name="reader">The <see cref="StreamReader"/> source.</param>
    /// <param name="sb">The <see cref="CharBuffer"/> target.</param>
    /// <returns>Returns false, if no characters were added, otherwise true.</returns>
    public static bool ReadLine(this StreamReader reader, CharBuffer sb)
    {
        var buffer = ArrayPool<char>.Shared.Rent(1024);

        int i = 0;

        while (true)
        {
            int ch = reader.Read();
            if (ch == -1) break;
            if (ch == '\r' || ch == '\n')
            {
                if (ch == '\r' && reader.Peek() == '\n') reader.Read();
                sb.Append(buffer, 0, i);
                ArrayPool<char>.Shared.Return(buffer);
                return true;
            }
            buffer[i++] = (char)ch;
            if (i == 1024)
            {
                sb.Append(buffer, 0 , 1024);
                i = 0;
            }
        }

        sb.Append(buffer, 0, i);
        ArrayPool<char>.Shared.Return(buffer);
        return sb.Length > 0;
    }

    /// <summary>
    /// Reads a line of characters from the text reader and adds it to the <see cref="StringBuilder"/> instance.
    /// </summary>
    /// <param name="reader">The <see cref="StreamReader"/> source.</param>
    /// <param name="sb">The <see cref="StringBuilder"/> target.</param>
    /// <returns>Returns false, if no characters were added, otherwise true.</returns>
    public static bool ReadLine(this StreamReader reader, StringBuilder sb)
    {
        var buffer = ArrayPool<char>.Shared.Rent(1024);

        int i = 0;

        while (true)
        {
            int ch = reader.Read();
            if (ch == -1) break;
            if (ch == '\r' || ch == '\n')
            {
                if (ch == '\r' && reader.Peek() == '\n') reader.Read();
                sb.Append(buffer, 0, i);
                ArrayPool<char>.Shared.Return(buffer);
                return true;
            }
            buffer[i++] = (char)ch;
            if (i == 1024)
            {
                sb.Append(buffer, 0, 1024);
                i = 0;
            }
        }

        sb.Append(buffer, 0, i);
        ArrayPool<char>.Shared.Return(buffer);
        return sb.Length > 0;
    }
}
