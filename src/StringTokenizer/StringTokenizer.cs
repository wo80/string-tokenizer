﻿namespace StringTokenizer;

using System;

public class StringTokenizer
{
    public readonly struct TextSpan
    {
        /// <summary>
        /// Gets the start position of the text span.
        /// </summary>
        public readonly int Start;

        /// <summary>
        /// Gets the end position of the text span.
        /// </summary>
        public readonly int End;

        /// <summary>
        /// Gets the length of the text span.
        /// </summary>
        public readonly int Length => End - Start;

        public TextSpan(int start, int end)
        {
            Start = start;
            End = end;
        }

        public bool IsEmpty() => Start == End;
    }

    #region Tokenizer Strategies

    public interface ITokenizerStrategy
    {
        /// <summary>
        /// Read the next token from the buffer.
        /// </summary>
        /// <param name="buffer">The character buffer.</param>
        /// <param name="position">The current position in the buffer.</param>
        /// <param name="length">The length of the current string (less or equal buffer length).</param>
        /// <param name="span">The <see cref="TextSpan"/>.</param>
        /// <returns>The new position in the buffer (might differ from TextSpan.End).</returns>
        int Next(char[] buffer, int position, int length, out TextSpan span);
    }

    /// <summary>
    /// Use whitespace as token delimiter.
    /// </summary>
    public class WhitespaceStrategy : ITokenizerStrategy
    {
        /// <inheritdoc/>
        public int Next(char[] buffer, int position, int length, out TextSpan span)
        {
            int i = position;

            // To match other ASCII control characters, instead of calling
            // char.IsWhiteSpace(c) we could check c < 33.

            // Skip whitespace
            while (i < length && char.IsWhiteSpace(buffer[i])) i++;

            int start = i;

            // Check for whitespace.
            while (i < length && !char.IsWhiteSpace(buffer[i])) i++;

            span = new TextSpan(start, i);

            return i;
        }
    }

    /// <summary>
    /// Use whitespace as token delimiter, but allow quoted tokens.
    /// </summary>
    public class QuotedWhitespaceStrategy : ITokenizerStrategy
    {
        private readonly char quoteOpen;
        private readonly char quoteClose;

        /// <summary>
        /// Initializes a new instance of the <see cref="QuotedWhitespaceStrategy"/> class.
        /// </summary>
        /// <param name="quote">The quote that escapes whitespace inside a token.</param>
        public QuotedWhitespaceStrategy(char quote)
            : this(quote, quote)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="QuotedWhitespaceStrategy"/> class.
        /// </summary>
        /// <param name="quote">The opening quote that escapes whitespace inside a token.</param>
        /// <param name="quote">The closing quote that escapes whitespace inside a token.</param>
        /// <exception cref="ArgumentException"></exception>
        public QuotedWhitespaceStrategy(char quoteOpen, char quoteClose)
        {
            if (char.IsWhiteSpace(quoteOpen))
            {
                throw new ArgumentException("Cannot use whitespace as opening quote.", nameof(quoteOpen));
            }

            if (char.IsWhiteSpace(quoteClose))
            {
                throw new ArgumentException("Cannot use whitespace as closing quote.", nameof(quoteClose));
            }

            this.quoteOpen = quoteOpen;
            this.quoteClose = quoteClose;
        }

        /// <inheritdoc/>
        public int Next(char[] buffer, int position, int length, out TextSpan span)
        {
            int i = position;

            // Skip whitespace
            while (i < length && char.IsWhiteSpace(buffer[i])) i++;

            bool quoted = (i < length && buffer[i] == quoteOpen);

            int start = i;

            if (quoted)
            {
                // Advance to position behind the opening quote.
                start = ++i;

                while (i < length && buffer[i] != quoteClose) i++;

                span = new TextSpan(start, i);

                // Advance to position behind the closing quote.
                i++;

                // TODO: should we throw, if no closing quote is found?
            }
            else
            {
                while (i < length && !char.IsWhiteSpace(buffer[i])) i++;

                span = new TextSpan(start, i);
            }

            return i;
        }
    }

    /// <summary>
    /// Use a delimiter to detect tokens.
    /// </summary>
    public class DelimiterStrategy : ITokenizerStrategy
    {
        private readonly char delimiter;
        private readonly bool trim;

        /// <summary>
        /// Initializes a new instance of the <see cref="DelimiterStrategy"/> class.
        /// </summary>
        /// <param name="delimiter">The delimiter to separate tokens.</param>
        public DelimiterStrategy(char delimiter)
            : this(delimiter, true)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DelimiterStrategy"/> class.
        /// </summary>
        /// <param name="delimiter">The delimiter to separate tokens.</param>
        /// <param name="trim">If true, automatically trim token text.</param>
        public DelimiterStrategy(char delimiter, bool trim)
        {
            this.delimiter = delimiter;
            this.trim = trim;
        }

        /// <inheritdoc/>
        public int Next(char[] buffer, int position, int length, out TextSpan span)
        {
            int i = position;

            if (trim)
            {
                // Skip whitespace
                while (i < length && char.IsWhiteSpace(buffer[i])) i++;
            }

            int start = i;

            while (i < length && buffer[i] != delimiter) i++;

            // Point to position after delimiter.
            position = i + 1;

            if (trim)
            {
                // Remove trailing whitespace.
                while (i > start && char.IsWhiteSpace(buffer[i - 1])) i--;
            }

            span = new TextSpan(start, i);

            if (position > length)
            {
                // The line might end with a delimiter or not. In case it
                // doesn't, lets adjust the position.
                position = length;
            }

            return position;
        }
    }

    #endregion

    private char[] buffer;

    private int position, length;

    private  ITokenizerStrategy strategy;

    /// <summary>
    /// Gets or sets the <see cref="ITokenizerStrategy"/>.
    /// </summary>
    public ITokenizerStrategy Strategy { get => strategy; set => strategy = value; }

    /// <summary>
    /// Initializes a new instance of the <see cref="StringTokenizer"/> class.
    /// </summary>
    public StringTokenizer()
        : this(null, 0)
    {
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="StringTokenizer"/> class.
    /// </summary>
    /// <param name="buffer">The <see cref="char"/> buffer.</param>
    public StringTokenizer(char[] buffer)
        : this(buffer, buffer.Length)
    {
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="StringTokenizer"/> class.
    /// </summary>
    /// <param name="buffer">The <see cref="char"/> buffer.</param>
    /// <param name="length">The length of the data inside the buffer.</param>
    public StringTokenizer(char[] buffer, int length)
    {
        this.buffer = buffer;
        this.length = length;

        strategy = new WhitespaceStrategy();
    }

    /// <summary>
    /// Set buffer and data length.
    /// </summary>
    /// <param name="buffer">The new buffer.</param>
    /// <param name="length">The length of the data inside the buffer.</param>
    public void Set(char[] buffer, int length)
    {
        this.buffer = buffer;

        Reset(length);
    }

    /// <summary>
    /// Reset the position and length of the buffer.
    /// </summary>
    /// <param name="length">The length of the data inside the buffer.</param>
    public void Reset(int length)
    {
        position = 0;

        this.length = length;
    }

    /// <summary>
    /// Return whether the buffer contains more data.
    /// </summary>
    /// <returns></returns>
    public bool HasNext()
    {
        return position < length;
    }

    /// <summary>
    /// Gets the next token.
    /// </summary>
    /// <returns>The <see cref="TextSpan"/> representing the current token.</returns>
    public TextSpan Next()
    {
        position = strategy.Next(buffer, position, length, out TextSpan span);

        return span;
    }

    /// <summary>
    /// Gets the next token as an <see cref="int"/>.
    /// </summary>
    /// <param name="value">The current token.</param>
    /// <returns>Returns true, if the token was read successfully.</returns>
    public bool Next(out int value)
    {
        position = strategy.Next(buffer, position, length, out TextSpan span);
        
        return Parse.Int32(buffer, span.Start, span.End, out value);
    }

    /// <summary>
    /// Gets the next token as a <see cref="double"/>.
    /// </summary>
    /// <param name="value">The current token.</param>
    /// <returns>Returns true, if the token was read successfully.</returns>
    public bool Next(out double value)
    {
        position = strategy.Next(buffer, position, length, out TextSpan span);
        
        return Parse.Double(buffer, span.Start, span.End, out value);
    }

    /// <summary>
    /// Gets the next token as a <see cref="string"/>.
    /// </summary>
    /// <param name="value">The current token.</param>
    /// <returns>Returns true, if the token was read successfully.</returns>
    public bool Next(out string value)
    {
        position = strategy.Next(buffer, position, length, out TextSpan span);

        value = new string(buffer, span.Start, span.Length);

        return true;
    }
}
