﻿namespace StringTokenizer;

using System.Text;

public class ByteBuffer : Buffer<byte>
{
    #region Constructors

    /// <inheritdoc/>
    public ByteBuffer()
        : base(DefaultCapacity, int.MaxValue)
    {
    }

    /// <inheritdoc/>
    public ByteBuffer(int capacity)
        : base(capacity, int.MaxValue)
    {
    }

    /// <inheritdoc/>
    public ByteBuffer(int capacity, int maxCapacity)
        : base(capacity, maxCapacity)
    {
    }

    /// <inheritdoc/>
    public ByteBuffer(byte[] value)
        : base(value, value?.Length ?? DefaultCapacity)
    {
    }

    /// <inheritdoc/>
    public ByteBuffer(byte[] value, int capacity)
        : base(value, capacity)
    {
    }

    #endregion

    /// <inheritdoc/>
    public override string ToString(Encoding encoding, int startIndex, int count, bool trim)
    {
        if (trim)
        {
            int k = 0;

            while (IsWhitespace(_buffer[startIndex + k]) && k < count) k++;

            startIndex += k;
            count -= k;

            while (IsWhitespace(_buffer[startIndex + count - 1]) && count > 0) count--;
        }

        return encoding.GetString(_buffer, startIndex, count);
    }

    private static bool IsWhitespace(byte b)
    {
        return b == ' ' || b == '\t' || b == '\r' || b == '\n';
    }
}
