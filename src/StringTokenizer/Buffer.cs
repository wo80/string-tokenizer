﻿namespace StringTokenizer;

using System;
using System.Runtime.CompilerServices;
using System.Text;

#if DEBUG
[System.Diagnostics.DebuggerDisplay("{DebuggerDisplay}")]
#endif
public abstract class Buffer<T> where T : struct, IEquatable<T>
{
    private readonly int _sizeOfT;

    /// <summary>
    /// The maximum capacity this buffer is allowed to have.
    /// </summary>
    private readonly int _maxCapacity;

    /// <summary>
    /// The default capacity of a <see cref="Buffer{T}"/>.
    /// </summary>
    protected const int DefaultCapacity = 128;

    protected T[] _buffer;

    protected int _length;

    /// <summary>
    /// Initializes a new instance of the <see cref="Buffer{T}"/> class.
    /// </summary>
    public Buffer()
        : this(DefaultCapacity, int.MaxValue)
    {
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="Buffer{T}"/> class.
    /// </summary>
    /// <param name="capacity">The initial capacity of this buffer.</param>
    public Buffer(int capacity)
        : this(capacity, int.MaxValue)
    {
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="Buffer{T}"/> class.
    /// </summary>
    /// <param name="capacity">The initial capacity of this buffer.</param>
    /// <param name="maxCapacity">The maximum capacity of this buffer.</param>
    public Buffer(int capacity, int maxCapacity)
    {
        if (capacity > maxCapacity)
        {
            throw new ArgumentOutOfRangeException(nameof(capacity));
        }
        if (maxCapacity < 1)
        {
            throw new ArgumentOutOfRangeException(nameof(maxCapacity));
        }
        if (capacity < 0)
        {
            throw new ArgumentOutOfRangeException(nameof(capacity));
        }

        if (capacity == 0)
        {
            capacity = Math.Min(DefaultCapacity, maxCapacity);
        }

        _sizeOfT = Unsafe.SizeOf<T>();

        _maxCapacity = maxCapacity;
        _length = 0;
#if NET5_0_OR_GREATER
        _buffer = GC.AllocateUninitializedArray<T>(capacity);
#else
        _buffer = new T[capacity];
#endif
    }


    /// <summary>
    /// Initializes a new instance of the <see cref="Buffer{T}"/> class.
    /// </summary>
    /// <param name="value">The initial contents of this buffer.</param>
    public Buffer(T[] value)
        : this(value, value?.Length ?? DefaultCapacity)
    {
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="Buffer{T}"/> class.
    /// </summary>
    /// <param name="value">The initial contents of this buffer.</param>
    /// <param name="capacity">The initial capacity of this buffer.</param>
    public Buffer(T[] value, int capacity)
    {
        _sizeOfT = Unsafe.SizeOf<T>();

        _maxCapacity = int.MaxValue;
        _length = 0;

        _buffer = new T[capacity];

        Append(value, 0, value?.Length ?? 0);
    }

    /// <summary>
    /// Gets the length of this buffer.
    /// </summary>
    public int Length => _length;

    /// <summary>
    /// Gets or sets the maximum number of bytes that can be contained
    /// in the memory allocated by this buffer.
    /// </summary>
    public int Capacity
    {
        get => _buffer.Length;
        set
        {
            if (value < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(value));
            }
            if (value > MaxCapacity)
            {
                throw new ArgumentOutOfRangeException(nameof(value));
            }
            if (value < _length)
            {
                throw new ArgumentOutOfRangeException(nameof(value));
            }

            if (_buffer.Length != value)
            {
#if NET5_0_OR_GREATER
                T[] newArray = GC.AllocateUninitializedArray<T>(value);
#else
                T[] newArray = new T[value];
#endif
                Array.Copy(_buffer, newArray, _length);
                _buffer = newArray;
            }
        }
    }

    /// <summary>
    /// Gets the maximum capacity this buffer is allowed to have.
    /// </summary>
    public int MaxCapacity => _maxCapacity;

    /// <summary>
    /// Gets the <see cref="T"/> array buffer.
    /// </summary>
    public T[] Data => _buffer;

    /// <summary>
    /// Ensures that the capacity of this buffer is at least the specified value.
    /// </summary>
    /// <param name="capacity">The new capacity for this buffer.</param>
    /// <remarks>
    /// If <paramref name="capacity"/> is less than or equal to the current capacity of
    /// this buffer, the capacity remains unchanged.
    /// </remarks>
    public int EnsureCapacity(int capacity)
    {
        if (capacity < 0)
        {
            throw new ArgumentOutOfRangeException(nameof(capacity));
        }

        if (_buffer.Length < capacity)
        {
            Capacity = capacity;
        }

        return _buffer.Length;
    }

    /// <summary>
    /// Appends a copy of the specified bytes to this instance.
    /// </summary>
    /// <param name="value">The bytes to append.</param>
    public void Append(T[] value)
    {
        Append(value, 0, value.Length);
    }

    /// <summary>
    /// Appends a copy of a specified byte range to this instance.
    /// </summary>
    /// <param name="value">The bytes to append.</param>
    /// <param name="startIndex">The starting position of the byte range within <paramref name="value" />.</param>
    /// <param name="count">The number of bytes in <paramref name="value" /> to append.</param>
    public void Append(T[] value, int startIndex, int count)
    {
        int minLength = _length + count;

        if (_buffer.Length < minLength)
        {
            // Try to at least double the buffer size.
            int newSize = Math.Max(2 * _buffer.Length, minLength);

            if (newSize > _maxCapacity)
            {
                // Try to at least provide enough space for new data.
                newSize = minLength;

                if (newSize > _maxCapacity)
                {
                    throw new InvalidOperationException("value exceeds max capacity");
                }
            }

            Array.Resize(ref _buffer, newSize);
        }

        Buffer.BlockCopy(value, startIndex * _sizeOfT, _buffer, _length * _sizeOfT, count * _sizeOfT);

        _length += count;
    }

    public void Append(ReadOnlySpan<T> value)
    {
        int count = value.Length;

        int minLength = _length + count;

        if (_buffer.Length < minLength)
        {
            EnsureCapacity(minLength);
        }

        for (int i = 0; i < count; i++)
        {
            _buffer[_length + i] = value[i];
        }

        _length += count;
    }

    /// <summary>
    /// Removes all bytes from the current <see cref="Buffer{T}" /> instance.
    /// </summary>
    public void Clear()
    {
        _length = 0;
    }

    /// <summary>
    /// Copies the bytes of this instance to the specified destination buffer.
    /// </summary>
    /// <param name="destination">The buffer where bytes will be copied.</param>
    public void CopyTo(Buffer<T> destination)
    {
        destination.EnsureCapacity(_length);

        CopyTo(0, destination._buffer, 0, _length);
    }

    /// <summary>
    /// Copies the bytes of this instance to the specified destination array.
    /// </summary>
    /// <param name="destination">The array where bytes will be copied.</param>
    public void CopyTo(T[] destination)
    {
        if (destination.Length < _length)
        {
            throw new ArgumentException(nameof(destination));
        }

        CopyTo(0, destination, 0, _length);
    }

    /// <summary>
    /// Copies the bytes from a specified segment of this instance to a
    /// specified segment of a destination array.
    /// </summary>
    /// <param name="sourceIndex">The starting position in this instance where bytes will be copied from.</param>
    /// <param name="destination">The array where bytes will be copied.</param>
    /// <param name="destinationIndex">The starting position in <paramref name="destination" /> where bytes will be copied.</param>
    /// <param name="count">The number of bytes to be copied.</param>
    public void CopyTo(int sourceIndex, T[] destination, int destinationIndex, int count)
    {
        if (destination == null)
        {
            throw new ArgumentNullException(nameof(destination));
        }

        if (count < 0)
        {
            throw new ArgumentOutOfRangeException(nameof(count));
        }

        if (destinationIndex < 0)
        {
            throw new ArgumentOutOfRangeException(nameof(destinationIndex));
        }

        if (destinationIndex > destination.Length - count)
        {
            throw new ArgumentException();
        }

        if ((uint)sourceIndex > _length)
        {
            throw new ArgumentOutOfRangeException(nameof(sourceIndex));
        }

        if (sourceIndex > _length - count)
        {
            throw new ArgumentException();
        }

        Buffer.BlockCopy(_buffer, sourceIndex, destination, destinationIndex, count);
    }

    /// <summary>
    /// Determines whether the beginning of this buffer matches the specified byte.
    /// </summary>
    /// <param name="value">The byte to compare.</param>
    /// <returns>True if <paramref name="value" /> matches the beginning of this string; otherwise, false.</returns>
    public bool StartsWith(T value)
    {
        return _length > 0 &&  _buffer[0].Equals(value);
    }

    /// <summary>
    /// Determines whether the beginning of this buffer matches the specified bytes.
    /// </summary>
    /// <param name="value">The bytes to compare.</param>
    /// <returns>True if <paramref name="value" /> matches the beginning of this string; otherwise, false.</returns>
    public bool StartsWith(T[] value)
    {
        int len = value.Length;

        if (len > _length) return false;

        for (int i = 0; i < len; i++)
        {
            if (!value[i].Equals(_buffer[i]))
            {
                return false;
            }
        }

        return true;
    }

    /// <summary>
    /// Determines whether the end of this buffer matches the specified bytes.
    /// </summary>
    /// <param name="value">The bytes to compare to the end of this instance.</param>
    /// <returns>True if <paramref name="value" /> matches the end of this instance; otherwise, false.</returns>
    public bool EndsWith(T[] value)
    {
        int len = value.Length;

        if (len > _length) return false;

        for (int i = len - 1, j = _length - 1; i >= 0; i--, j--)
        {
            if (!value[i].Equals(_buffer[j]))
            {
                return false;
            }
        }

        return true;
    }

    /// <summary>Returns a value indicating whether a specified byte array occurs within this buffer.</summary>
    /// <param name="value">The bytes to seek.</param>
    /// <returns>True if the <paramref name="value" /> parameter occurs within this buffer; otherwise, false.</returns>
    public bool Contains(T[] value)
    {
        return IndexOf(value, 0, _length) >= 0;
    }

    /// <summary>Reports the zero-based index of the first occurrence of the specified byte in this buffer.</summary>
    /// <param name="value">The byte to seek.</param>
    /// <returns>The position of <paramref name="value" /> if the byte is found, or -1 if it is not.</returns>
    public int IndexOf(T value)
    {
        return IndexOf(value, 0);
    }

    /// <summary>Reports the zero-based index of the first occurrence of the specified byte in this buffer. The search starts at a specified character position.</summary>
    /// <param name="value">The byte to seek.</param>
    /// <param name="startIndex">The search starting position.</param>
    /// <returns>The position of <paramref name="value" /> if the byte is found, or -1 if it is not.</returns>
    public int IndexOf(T value, int startIndex)
    {
        for (int i = startIndex; i < _length; i++)
        {
            if (_buffer[i].Equals(value))
            {
                return i;
            }
        }

        return -1;
    }

    /// <summary>Reports the zero-based index of the first occurrence of the specified byte array in this buffer.</summary>
    /// <param name="value">The bytes to seek.</param>
    /// <returns>The position of <paramref name="value" /> if the bytes are found, or -1 if it is not.</returns>
    public int IndexOf(T[] value)
    {
        return IndexOf(value, 0, _length);
    }

    /// <summary>Reports the zero-based index of the first occurrence of the specified byte array in this buffer. The search starts at a specified character position.</summary>
    /// <param name="value">The bytes to seek.</param>
    /// <param name="startIndex">The search starting position.</param>
    /// <returns>The position of <paramref name="value" /> if the bytes are found, or -1 if it is not.</returns>
    public int IndexOf(T[] value, int startIndex)
    {
        return IndexOf(value, startIndex, _length);
    }

    /// <summary>Reports the zero-based index of the first occurrence of the specified byte array in this buffer. The search starts at a specified character position.</summary>
    /// <param name="value">The bytes to seek.</param>
    /// <param name="startIndex">The search starting position.</param>
    /// <param name="count">The number of byte positions to examine.</param>
    /// <returns>The position of <paramref name="value" /> if the bytes are found, or -1 if it is not.</returns>
    private int IndexOf(T[] value, int startIndex, int count)
    {
        int bufferLen = _length;

        if (value == null)
        {
            throw new ArgumentNullException(nameof(value));
        }

        if (startIndex > bufferLen)
        {
            throw new ArgumentOutOfRangeException(nameof(startIndex));
        }

        if (bufferLen == 0)
        {
            return value.Length == 0 ? 0 : -1;
        }

        if (startIndex < 0)
        {
            throw new ArgumentOutOfRangeException(nameof(startIndex));
        }

        if (count < 0 || startIndex > bufferLen - count)
        {
            throw new ArgumentOutOfRangeException(nameof(count));
        }

        int end = startIndex + count;
        int limit = end - value.Length;
        while (startIndex <= limit)
        {
            if (Compare(_buffer, startIndex, value, 0, value.Length))
            {
                return startIndex;
            }
            startIndex++;
        }

        /*
        byte a = value[0];
        while (startIndex <= limit)
        {
            // Performance optimization: test first byte match before calling Compare(...)
            if (buffer[startIndex] == a && Compare(buffer, startIndex + 1, value, 1, value.Length))
            {
                return startIndex;
            }
            startIndex++;
        }
        //*/

        return -1;
    }
    /// <summary>Compares ranges of two specified byte arrays.</summary>
    /// <param name="bufferA">The first byte array to use in the comparison.</param>
    /// <param name="indexA">The starting position of the range within <paramref name="bufferA" />.</param>
    /// <param name="bufferB">The second byte array to use in the comparison.</param>
    /// <param name="indexB">The starting position of the range within <paramref name="bufferB" />.</param>
    private bool Compare(T[] bufferA, int indexA, T[] bufferB, int indexB, int lengthB)
    {
        for (int i = 0; i < lengthB; i++)
        {
            if (!bufferA[indexA + i].Equals(bufferB[indexB + i]))
            {
                return false;
            }
        }
        return true;
    }

    /// <summary>
    /// Return the buffer content as a <see cref="ReadOnlySpan{T}"/>.
    /// </summary>
    public ReadOnlySpan<T> AsSpan()
    {
        return new ReadOnlySpan<T>(_buffer, 0, _length);
    }

    public override string ToString()
    {
        return ToString(Encoding.ASCII, 0, _length);
    }

    public string ToString(Encoding encoding)
    {
        return ToString(encoding, 0, _length, false);
    }

    public string ToString(Encoding encoding, int startIndex)
    {
        return ToString(encoding, startIndex, _length - startIndex, false);
    }

    public string ToString(Encoding encoding, int startIndex, bool trim)
    {
        return ToString(encoding, startIndex, _length - startIndex, trim);
    }

    public string ToString(Encoding encoding, int startIndex, int count)
    {
        return ToString(encoding, startIndex, count, false);
    }

    public abstract string ToString(Encoding encoding, int startIndex, int count, bool trim);

#if DEBUG
    internal string DebuggerDisplay => ToString();
#endif
}
