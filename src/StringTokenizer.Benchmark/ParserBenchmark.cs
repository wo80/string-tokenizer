﻿namespace StringTokenizer.Benchmark;

using System;
using System.Diagnostics;

class ParserBenchmark
{
    public static double ErrorTolerance = 1e-15;

    public static int Seed = 52061;

    public static void Run(int count)
    {
        Console.Write("Generating {0} test numbers   0%", count);

        var values = GenerateNumbers(count, true);

        Console.WriteLine();
        Console.WriteLine("Running benchmark ...", count);

        var timer = new Stopwatch();

        var expected = new double[count];
        var actual = new double[count];

        var t1 = TestNetParser(values, expected, timer);
        var t2 = TestCustomParser(values, actual, timer);

        Console.WriteLine();
        Console.WriteLine("double.Parse (.NET)  : {0:0.000,8} s", TimeSpan.FromTicks(t1).TotalSeconds);
        Console.WriteLine("Parse.Double (custom): {0:0.000,8} s", TimeSpan.FromTicks(t2).TotalSeconds);

        int mismatch = 0;

        double error, tolerance = ErrorTolerance;
        double error_max = 0.0;

        bool report = false;

        for (int i = 0; i < count; i++)
        {
            // Compute the relative error.
            error = Math.Abs(expected[i] - actual[i]) / Math.Abs(expected[i]);

            if (error > error_max) error_max = error;

            if (error > tolerance)
            {
                mismatch++;

                if (report)
                {
                    Console.WriteLine("[{0}]: string = {1} > {2} <> {3}", i, values[i], expected[i], actual[i]);
                }
            }
        }

        if (mismatch > 0)
        {
            Console.WriteLine("Encountered {0} horrors! Max. error = {1}", mismatch, error_max);
        }
    }

    private static long TestNetParser(string[] input, double[] output, Stopwatch timer)
    {
        var nfi = System.Globalization.NumberFormatInfo.InvariantInfo;

        timer.Reset();

        for (int i = 0; i < input.Length; i++)
        {
            timer.Start();
            output[i] = double.Parse(input[i], nfi);
            timer.Stop();
        }

        return timer.ElapsedTicks;
    }

    private static long TestCustomParser(string[] input, double[] output, Stopwatch timer)
    {
        timer.Reset();

        for (int i = 0; i < input.Length; i++)
        {
            timer.Start();
            Parse.Double(input[i], out double a);
            timer.Stop();

            output[i] = a;
        }

        return timer.ElapsedTicks;
    }

    private static string[] GenerateNumbers(int count, bool normalized)
    {
        var rng = new Random(Seed);

        var values = new string[count];

        int step = count / 10;

        for (int i = 0; i < count; i++)
        {
            var lval = normalized ? rng.Next(0, 9).ToString() : GenerateNumberSequence(rng.Next(1, 10), rng);
            var rval = GenerateNumberSequence(normalized ? rng.Next(10, 30) : rng.Next(1, 30), rng);

            var sign = rng.Next(1, 10) > 5 ? "" : "-";
            var expsign = rng.Next(1, 10) > 5 ? "" : "-";

            var exp = (normalized && rng.Next(1, 10) > 5) ? "E" + expsign + rng.Next(1, 16) : "";

            values[i] = sign + lval + "." + rval + exp;

            if ((i % step) == 0)
            {
                Console.Write("\b\b\b\b\b");
                Console.Write(" {0,3}%", (int)(100f * i / count));
            }
        }

        Console.Write("\b\b\b\b\b");
        Console.WriteLine(" 100%");

        return values;
    }

    private static string GenerateNumberSequence(int length, Random rng)
    {
        var number = "";

        for (int i = 0; i < length; i++)
        {
            number += rng.Next(0, 9).ToString();
        }

        return number;
    }
}
