﻿namespace StringTokenizer.Benchmark;

using CSparse.Storage;
using System;
using System.IO;
using System.Numerics;

enum MatrixMarketSymmetry
{
    General,
    Symmetric,
    SkewSymmetric,
    Hermitian
}

/// <summary>
/// Read files in Matrix Market format (only supports coordinate storage).
/// </summary>
public class CustomMatrixMarketReader
{
    private static readonly char[] separator = new char[] { ' ', '\t' };

    /// <summary>
    /// Read a matrix from file.
    /// </summary>
    public static CompressedColumnStorage<T> ReadMatrix<T>(string filePath)
        where T : struct, IEquatable<T>, IFormattable
    {
        using var stream = File.OpenRead(filePath);

        return ReadMatrix<T>(stream);
    }

    /// <summary>
    /// Read a matrix from stream.
    /// </summary>
    public static CompressedColumnStorage<T> ReadMatrix<T>(Stream stream)
        where T : struct, IEquatable<T>, IFormattable
    {
        using var reader = new StreamReader(stream);

        return CompressedColumnStorage<T>.OfIndexed(ReadStorage<T>(reader));
    }

    /// <summary>
    /// Read coordinate storage from a text reader.
    /// </summary>
    public static CoordinateStorage<T> ReadStorage<T>(StreamReader stream, bool autoExpand = true)
        where T : struct, IEquatable<T>, IFormattable
    {
        ExpectHeader(stream, true, out bool complex, out bool sparse, out bool pattern, out MatrixMarketSymmetry symmetry);

        var parse = CreateValueParser<T>(complex, pattern);

        var sizes = ExpectLine(stream).Split(separator, StringSplitOptions.RemoveEmptyEntries);

        int rows = int.Parse(sizes[0]);
        int cols = int.Parse(sizes[1]);

        if (sparse)
        {
            int nnz = int.Parse(sizes[2]);

            var storage = new CoordinateStorage<T>(rows, cols, nnz);

            var s = BuildStorage(stream, storage, parse);

            bool lower = s.Item1;
            bool upper = s.Item2;

            // The Matrix Market specification states that symmetric/Hermitian matrices
            // should only store the lower triangular part, so we have to expand the
            // storage manually:
            if (autoExpand && symmetry != MatrixMarketSymmetry.General && !upper)
            {
                ExpandStorage(symmetry, storage);
            }

            return storage;
        }

        throw new NotSupportedException();
    }

    static void ExpandStorage<T>(MatrixMarketSymmetry symmetry, CoordinateStorage<T> storage)
        where T : struct, IEquatable<T>, IFormattable
    {
        var map = CreateSymmetryMap<T>(symmetry);

        var ai = storage.RowIndices;
        var aj = storage.ColumnIndices;
        var ax = storage.Values;

        int i, j, n = storage.NonZerosCount;

        for (int k = 0; k < n; k++)
        {
            i = ai[k];
            j = aj[k];

            if (i > j)
            {
                storage.At(j, i, map(ax[k]));
            }
        }
    }

    #region Helper

    static void ExpectHeader(TextReader reader, bool matrix, out bool complex, out bool sparse, out bool pattern, out MatrixMarketSymmetry symmetry)
    {
        complex = pattern = false;
        string line;
        while ((line = reader.ReadLine()) != null)
        {
            line = line.Trim();
            if (line.StartsWith("%%MatrixMarket"))
            {
                var tokens = line.ToLowerInvariant().Substring(15).Split(separator, StringSplitOptions.RemoveEmptyEntries);
                if (tokens.Length < 2)
                {
                    throw new FormatException(@"Expected MatrixMarket Header with 2-4 attributes: object format [field] [symmetry]; see http://math.nist.gov/MatrixMarket/ for details.");
                }

                if (tokens[0] != (matrix ? "matrix" : "vector"))
                {
                    throw new FormatException("Expected matrix content.");
                }

                switch (tokens[1])
                {
                    case "array":
                        sparse = false;
                        break;
                    case "coordinate":
                        sparse = true;
                        break;
                    default:
                        throw new NotSupportedException("Format type not supported.");
                }

                if (tokens.Length < 3)
                {
                    complex = false;
                }
                else
                {
                    switch (tokens[2])
                    {
                        case "real":
                        case "double":
                        case "integer":
                            complex = false;
                            break;
                        case "complex":
                            complex = true;
                            break;
                        case "pattern":
                            pattern = true;
                            break;
                        default:
                            throw new NotSupportedException("Field type not supported.");
                    }
                }

                if (tokens.Length < 4)
                {
                    symmetry = MatrixMarketSymmetry.General;
                }
                else
                {
                    switch (tokens[3])
                    {
                        case "general":
                            symmetry = MatrixMarketSymmetry.General;
                            break;
                        case "symmetric":
                            symmetry = MatrixMarketSymmetry.Symmetric;
                            break;
                        case "skew-symmetric":
                            symmetry = MatrixMarketSymmetry.SkewSymmetric;
                            break;
                        case "hermitian":
                            symmetry = MatrixMarketSymmetry.Hermitian;
                            break;
                        default:
                            throw new NotSupportedException("Symmetry type not supported");
                    }
                }

                return;
            }
        }

        throw new FormatException(@"Expected MatrixMarket Header, see http://math.nist.gov/MatrixMarket/ for details.");
    }

    static string ExpectLine(TextReader reader)
    {
        string line;
        while ((line = reader.ReadLine()) != null)
        {
            var trim = line.Trim();
            if (trim.Length > 0 && !trim.StartsWith("%"))
            {
                return trim;
            }
        }

        throw new FormatException(@"End of file reached unexpectedly.");
    }

    static Func<StringTokenizer, T> CreateValueParser<T>(bool sourceIsComplex, bool pattern)
    {
        if (pattern)
        {
            return (tokenizer) => (T)(object)1.0;
        }

        if (typeof(T) == typeof(double))
        {
            // Ignore imaginary part if source is complex.
            return (tokenizer) =>
            {
                tokenizer.Next(out double value);

                return (T)(object)value;
            };
        }

        if (typeof(T) == typeof(Complex))
        {
            if (sourceIsComplex)
            {
                return (tokenizer) =>
                {
                    tokenizer.Next(out double re);
                    tokenizer.Next(out double im);

                    return (T)(object)new Complex(re, im);
                };
            }

            return (tokenizer) =>
            {
                tokenizer.Next(out double re);

                return (T)(object)new Complex(re, 0d);
            };
        }

        throw new NotSupportedException();
    }

    static Func<T, T> CreateSymmetryMap<T>(MatrixMarketSymmetry symmetry)
    {
        if (symmetry != MatrixMarketSymmetry.Hermitian)
        {
            return x => x;
        }

        if (typeof(T) == typeof(double))
        {
            return x => x;
        }

        if (typeof(T) == typeof(Complex))
        {
            return x => (T)(object)Complex.Conjugate((Complex)(object)x);
        }

        throw new NotSupportedException();
    }

    private static Tuple<bool, bool> BuildStorage<T>(StreamReader reader, CoordinateStorage<T> storage, Func<StringTokenizer, T> parse)
        where T : struct, IEquatable<T>, IFormattable
    {
        try
        {
            bool lower = false;
            bool upper = false;

            var buffer = new CharBuffer(1024);

            var tokenizer = new StringTokenizer();

            while (reader.ReadLine(buffer))
            {
                if (buffer.StartsWith('%')) continue;

                tokenizer.Set(buffer.Data, buffer.Length);

                // Process line
                tokenizer.Next(out int i);
                tokenizer.Next(out int j);

                // MatrixMarket indices are 1-based.
                storage.At(i - 1, j - 1, parse(tokenizer));

                if (i > j)
                {
                    lower = true;
                }

                if (i < j)
                {
                    upper = true;
                }

                buffer.Clear();
            }

            return new Tuple<bool, bool>(lower, upper);
        }
        catch (Exception exception)
        {
            throw new Exception("File could not be parsed", exception);
        }
        /*
        try
        {
            bool lower = false;
            bool upper = false;

            var sb = new StringBuilder(1024);

            var buffer = new char[1024];

            var tokenizer = new StringTokenizer(buffer);

            while (reader.ReadLine(sb))
            {
                int length = sb.Length;

                if (buffer.Length < length - 1)
                {
                    buffer = new char[buffer.Length * 2];
                }

                sb.CopyTo(0, buffer, 0, length);
                sb.Clear();

                buffer[length] = '\0';

                // Process line
                tokenizer.Reset(length);

                if (buffer[0] != '%')
                {
                    tokenizer.Next(out int i);
                    tokenizer.Next(out int j);

                    // MatrixMarket indices are 1-based.
                    storage.At(i - 1, j - 1, parse(tokenizer));

                    if (i > j)
                    {
                        lower = true;
                    }

                    if (i < j)
                    {
                        upper = true;
                    }
                }
            }

            return new Tuple<bool, bool>(lower, upper);
        }
        catch (Exception exception)
        {
            throw new Exception("File could not be parsed", exception);
        }
        //*/
    }

    #endregion
}
