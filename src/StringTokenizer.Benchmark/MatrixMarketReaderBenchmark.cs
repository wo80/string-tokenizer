﻿namespace StringTokenizer.Benchmark;

using CSparse;
using CSparse.IO;
using CSparse.Storage;
using System;
using System.Diagnostics;
using System.IO;

internal class MatrixMarketReaderBenchmark
{
    // https://sparse.tamu.edu/FIDAP/ex11

    public static Matrix<double> TestCSparse(string file, bool monitor = true)
    {
        Console.WriteLine("Test CSparse MatrixMarketReader");

        file ??= CreateTestFile();

        var fi = new FileInfo(file);

        if (!fi.Exists)
        {
            throw new Exception("File not found: " + file);
        }

        Console.WriteLine("File Name: {0}", file);
        Console.WriteLine("File Size: {0:0.0} kb", fi.Length / 1024.0);

        if (monitor)
        {
            AppDomain.MonitoringIsEnabled = true;
        }

        var timer = new Stopwatch();

        timer.Start();

        var A = MatrixMarketReader.ReadMatrix<double>(file);

        timer.Stop();

        Console.WriteLine("Time: {0}", timer.Elapsed);
        Console.WriteLine();

        if (monitor)
        {
            PrintMonitorResults(A);
        }

        return A;
    }

    public static Matrix<double> TestCustom(string file, bool monitor = true)
    {
        Console.WriteLine("Test custom MatrixMarketReader");

        file ??= CreateTestFile();

        var fi = new FileInfo(file);

        if (!fi.Exists)
        {
            throw new Exception("File not found: " + file);
        }

        Console.WriteLine("File Name: {0}", file);
        Console.WriteLine("File Size: {0} kb", fi.Length / 1024);

        if (monitor)
        {
            AppDomain.MonitoringIsEnabled = true;
        }

        var timer = new Stopwatch();

        timer.Start();

        var A = CustomMatrixMarketReader.ReadMatrix<double>(file);

        timer.Stop();

        Console.WriteLine("Time: {0}", timer.Elapsed);
        Console.WriteLine();

        if (monitor)
        {
            PrintMonitorResults(A);
        }

        return A;
    }

    public static void Compare()
    {
        string file = CreateTestFile();

        Compare(file);
    }

    public static void Compare(string file)
    {
        file ??= CreateTestFile();

        var A = TestCSparse(file, false);
        var B = TestCustom(file, false);

        if (!A.Equals(B, 1e-6))
        {
            Console.WriteLine("ERROR: matrices are not equal.");
        }
    }

    private static string CreateTestFile()
    {
        string file = "test.mtx";

        CreateTestFile(file, 10000, 10000, 0.02);

        return file;
    }

    private static void CreateTestFile(string file, int rows, int columns, double density)
    {
        if (File.Exists(file)) return;

        Console.WriteLine("Creating file '" + file + "' ...");

        int nz = (int)(rows * (columns * density));

        var s = new CoordinateStorage<double>(rows, columns, nz);
        var r = Random.Shared;

        for (int i = 0; i < nz; i++)
        {
            s.At(r.Next(rows), r.Next(columns), r.NextDouble());
        }

        MatrixMarketWriter.WriteMatrix(file, CompressedColumnStorage<double>.OfIndexed(s));

        Console.WriteLine();
    }

    private static void PrintMonitorResults(CompressedColumnStorage<double> A)
    {
        Console.WriteLine("Matrix: {0} x {1}, {2} non-zeros, Memory: {3} kb", A.RowCount, A.ColumnCount, A.NonZerosCount, GetBytes(A) / 1024);
        Console.WriteLine("Total Processor Time: {0:0.0} ms", AppDomain.CurrentDomain.MonitoringTotalProcessorTime.TotalMilliseconds);
        Console.WriteLine("Allocated Memory: {0} kb", AppDomain.CurrentDomain.MonitoringTotalAllocatedMemorySize / 1024);
        Console.WriteLine("Peak Working Set: {0} kb", Process.GetCurrentProcess().PeakWorkingSet64 / 1024);

        for (var index = 0; index <= GC.MaxGeneration; index++)
        {
            Console.WriteLine("Gen {0} collections: {1}", index, GC.CollectionCount(index));
        }

        //GC.Collect();
        //Console.WriteLine("Current Working Set: {0:0.0} kb", Process.GetCurrentProcess().WorkingSet64 / 1024);
    }

    private static long GetBytes(Matrix<double> A)
    {
        var S = A as CSparse.Double.SparseMatrix;

        if (S == null) return 0L;

        var n = S.ColumnCount;
        var nz = S.NonZerosCount;

        return (n + 1) * sizeof(int) + nz * sizeof(int) + nz * sizeof(double);
    }
}
