﻿namespace StringTokenizer.Benchmark;

using System;
using System.IO;

class Program
{
    static void Main(string[] args)
    {
        //new LineReaderTest().RunTest();
        //return;

        if (args.Length == 0)
        {
            ParserBenchmark.Run(1000000);
            return;
        }

        var a = args[0];

        if (a != "-b")
        {
            Help();
        }
        else if (args.Length == 1)
        {
            MatrixMarketReaderBenchmark.Compare();
        }
        else
        {
            var b = args[1];
            var c = args.Length > 2 ? args[2] : null;

            if (File.Exists(b))
            {
                MatrixMarketReaderBenchmark.Compare(b);
            }
            else if (b == "custom")
            {
                MatrixMarketReaderBenchmark.TestCustom(c);
            }
            else if (b == "csparse")
            {
                MatrixMarketReaderBenchmark.TestCSparse(c);
            }
            else
            {
                Help();
            }
        }
    }

    private static void Help()
    {
        Console.WriteLine("Benchmark command line:");
        Console.WriteLine();
        Console.WriteLine("benchmark");
        Console.WriteLine("   Run number parser benchmark.");
        Console.WriteLine();
        Console.WriteLine("benchmark -b [csparse|custom] [FILE]");
        Console.WriteLine("   Load MatrixMarket file.");
        Console.WriteLine();
        Console.WriteLine("   If FILE is not specified, a test file will be created.");
        Console.WriteLine("   If no method (csparse or custom) is specified, both will be");
        Console.WriteLine("   tested, but without memory statistics.");
    }
}
