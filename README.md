# A String Tokenizer for .NET

A simple string tokenizer written in C#. This repository also provides an advanced `ByteBuffer` class and a simple, efficient number parser. For details see

* [Efficiently reading large text files with .NET](http://wo80.bplaced.net/posts/2021/11-string-tokenizer/)

## Example

A simple unit test:

```csharp
var s = "1 \"2 3\" 4.5".ToCharArray();

var tokenizer = new StringTokenizer(s)
{
    Strategy = new StringTokenizer.QuotedWhitespaceStrategy('"')
};

Assert.That(tokenizer.Next(out double ival));
Assert.That(ival, Is.EqualTo(1));

Assert.That(tokenizer.Next(out string sval));
Assert.That(sval, Is.EqualTo("2 3"));

Assert.That(tokenizer.Next(out double dval));
Assert.That(dval, Is.EqualTo(4.5));

Assert.That(!tokenizer.HasNext());
```

Take a look at the other [unit tests](src/StringTokenizer.Tests) for more examples.
